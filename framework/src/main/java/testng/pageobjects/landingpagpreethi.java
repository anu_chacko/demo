package testng.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class landingpagpreethi {
	WebDriver driver;
	
	public landingpagpreethi(WebDriver driver)
	{
		
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	
	
	//WebElement userEmail =driver.findElement(By.id("userEmail"));
	//PageFActory
	
	
	@FindBy(css="a[class='my-account-link dropdown-toggle'] i[class='fa fa-user']")
	WebElement icon;
	
	@FindBy(xpath="//div[@class='my-account dropdown open']//a[normalize-space()='Login']")
	WebElement loginclick;
	
	@FindBy(xpath="//a[normalize-space()='Login with email']")
	WebElement login;
	
	@FindBy(name="email")
	WebElement userEmail;
	
	@FindBy(id="input-password")
	WebElement passwrd;
	
	@FindBy(xpath="//div[@class='form-group text-center']//input[@value='Login']")
	WebElement submit;
	
	
	public void applicationloginclick()
	{
		icon.click();
		loginclick.click();
		login.click();
	}
	public void loginapplication(String email, String password)
	{
		
		userEmail.sendKeys(email);
		passwrd.sendKeys(password);
		submit.click();
		
		
	}
	
	public void goTo()
	{
		driver.get("https://www.shop.preethi.in/");

	}
	
}


