package testng.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import testng.AbstractComponent.AbstractComponent;



public class LandingPage extends AbstractComponent {

	WebDriver driver;
	
	public  LandingPage(WebDriver driver)
	{
		super(driver);
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	
	
	//WebElement userEmail =driver.findElement(By.id("userEmail"));
	//PageFActory
	@FindBy(id="userEmail")
	WebElement userEmail;
	
	@FindBy(id="userPassword")
	WebElement passwordele;
	
	@FindBy(id="login")
	WebElement submit;
	
	public void loginapplication(String email, String password)
	{
		userEmail.sendKeys(email);
		passwordele.sendKeys(password);
		submit.click();
		
		
	}
	
	public void goTo()
	{
		driver.get("https://rahulshettyacademy.com/client");

	}
	
}
