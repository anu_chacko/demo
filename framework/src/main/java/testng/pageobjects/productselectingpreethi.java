package testng.pageobjects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class productselectingpreethi {


WebDriver driver;
	
	public productselectingpreethi(WebDriver driver)
	{
		
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	
	
	@FindBy(xpath="//strong[normalize-space()='Chimney']")
	WebElement slctingprd;
	
	@FindBy(xpath="//body/div[@class='container-fluid splug-container ']/div[@id='splug-content']/div[@id='content']/div[@id='ajax-product-list']/div[2]/div[1]/div[1]/div[2]/div[1]")
	WebElement slctingprdclick;
	
	
	
	
	
	public void prdctselectionclick()
	{
		slctingprd.click();
		 slctingprdclick.click();
		
	}
	
	

	public void redirectingnextpage()
	{
		List<String> tab2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tab2.get(1));
	}
	
}


