package testng.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class prdctaddtocartpreethi {
WebDriver driver;
	
	public  prdctaddtocartpreethi(WebDriver driver)
	{
		
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="button-cart")
	WebElement button;
	
	@FindBy(css=".pe-7s-cart")
	WebElement buttoncart;
	
	@FindBy(css=".btn.btcn-success.checkout-btn")
	WebElement checkout;
	
	@FindBy(name="payment_method")
	WebElement paymntbuttn;
	
	@FindBy(id="button-payment-method")
	WebElement buttnmthod;
	
	public void addtocart()
	{
		button.click();
		buttoncart.click();
		checkout.click();
	}
	
	public void payment()
	{
		paymntbuttn.click();
		//buttnmthod.click();
	}
	
	}
