package testng;

import java.time.Duration;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class amazonp1 extends amazonp2 {
	RemoteWebDriver driver;
	
	@BeforeTest
	public void Start()
	{
		
		ChromeOptions options=new ChromeOptions();
		options.addArguments("--remote-allow-origins=*");
		driver=new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
		driver.manage().window().maximize();
		PageFactory.initElements(driver,amazonp2.class);
		amazonp2 page=new amazonp2();
		page.Go(driver);
	}
	

	
	
	
	@Test
	public void HomePage()
	{
		amazonp2.home.click();
	}
	
	@Test
	public void ValidMobile()
	
	{
		
		amazonp2.data.sendKeys("8606597465");
		amazonp2.contin.click();
		driver.navigate().back();
		amazonp2.data.clear();
		
	}
	@Test
	public void ValidEmail()
	{
		amazonp2.data.sendKeys("anusuryachacko@gmail.com");
		amazonp2.contin.click();
		driver.navigate().back();
		amazonp2.data.clear();
		
	}
	
	public void invalidemail()
	{
		amazonp2.data.sendKeys("anusuryachackogmail.com");
		amazonp2.contin.click();
		String ob=amazonp2.error.getText();
		System.out.println(ob);
		driver.navigate().back();
		amazonp2.data.clear();
		
	}
	
	
	@Test
	public void alphabets()
	{
		amazonp2.data.sendKeys("dfksjfkfh");
		amazonp2.contin.click();
		String ob=amazonp2.error.getText();
		System.out.println(ob);
		driver.navigate().back();
		amazonp2.data.clear();
	}
	@Test
	public void nolessthanten()
	{
		amazonp2.data.sendKeys("89123456");
		amazonp2.contin.click();
		String ob=amazonp2.error.getText();
		System.out.println(ob);
		driver.navigate().back();
		amazonp2.data.clear();
	}
	@Test
	public void nogreaterthanten()
	{
		amazonp2.data.sendKeys("86065974651");
		amazonp2.contin.click();
		String ob=amazonp2.error.getText();
		System.out.println(ob);
		driver.navigate().back();
		amazonp2.data.clear();
		
		
	}
	@AfterTest
	public void After()
	{
		driver.quit();
	}
	
	
}
