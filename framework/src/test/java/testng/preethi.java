package testng;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import testng.pageobjects.landingpagpreethi;
import testng.pageobjects.prdctaddtocartpreethi;
import testng.pageobjects.productselectingpreethi;
//sonalint 
//jenkins
//cucumber report
// html report 


public class preethi  {
	WebDriver driver;


	@BeforeTest
	public void before() 
	{
		//System.setProperty("webdriver.chrome.driver", "C:\\Users\\Anusurya.Chacko\\Downloads\\chromedriver_win32\\chromedriver.exe");		
		driver= new ChromeDriver();
		driver.get("https://www.shop.preethi.in/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	}
	
	
	
	@DataProvider(name ="details")
	public String[][] ProductDataprovider()
	{
		return new String[][] {{"anusuryachacko@gmail.com","Angel12@"}};
	}
	
	
	
	@Test(dataProvider="details")
	public void landing(String s,String s1) 
	{
		
		landingpagpreethi landingPage=new landingpagpreethi(driver);
		landingPage.goTo();
		landingPage.applicationloginclick();
		landingPage.loginapplication(s,s1);
	}
	
	
	
	@Test(dataProvider="details")
	public void productselect(String s,String s1) throws InterruptedException 
	{
		//landing(s,s1);
		productselectingpreethi productselection=new productselectingpreethi(driver);
		productselection.prdctselectionclick();
		productselection.redirectingnextpage();
		Thread.sleep(1000);
	}
	
	
	@Test(dataProvider="details")
	public void addToCart(String s,String s1) throws InterruptedException
	{
		landing(s,s1);
		productselect(s,s1);
		prdctaddtocartpreethi prdctaddtocart =new prdctaddtocartpreethi(driver);		
		prdctaddtocart.addtocart();
		Thread.sleep(3000);
		prdctaddtocart.payment();
		Thread.sleep(3000);
	}
	
	
	@AfterTest
	public void After()
	{
		driver.quit();
	}
	
}




